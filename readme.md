### How to use?

- Clone or download this repo
- Enter to the project directory
- Run `npm install`
- Run `npm run start`
- Open browser and go to address `http://localhost:8001`
- Scan the QR Code

# List API Documentation

## Chat
<u>Sending Messages</u>
``POST : /send-message``
> Request Body
> - number - contains the number to be sent
> - message - contains the message to be sent
<hr>

## Send Media
<u>Sending Messages</u>
``POST : /send-media``
> Request Body
> - number - contains the number to be sent
> - caption - contains the caption to be sent
> - file - contains the file to be sent
<hr>

## Get Chat
<u>Get Chats</u>
``GET : /getchats``
<hr>

## And More
<u>you cant take a look in wweb.js</u>
``VISIT : https://wwebjs.dev/``
<hr>
